/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

/**
 *
 * @author Kyoo
 */
public class sessionLogin {
    private static String session_nik;
    private static String session_nama;
    private static String session_level;
    private static String session_password;
    
    public static String getsession_nik() {
        return session_nik;
    }    
    public static void setsession_nik(String session_nik){
        sessionLogin.session_nik = session_nik;
    }
    
    public static String getsession_nama() {
        return session_nama;
    }    
    public static void setsession_nama(String session_nama){
        sessionLogin.session_nama = session_nama;
    }
    
    public static String getsession_level() {
        return session_level;
    }    
    public static void setsession_level(String session_level){
        sessionLogin.session_level = session_level;
    }
    
    
    public static String getsession_password() {
        return session_password;
    }    
    public static void setsession_password(String session_password){
        sessionLogin.session_password = session_password;
    }
}
